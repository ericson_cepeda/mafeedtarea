<?php
include_once 'src/Epi.php';
include_once 'controllers/home.class.php';
include_once 'lib/constants.class.php';

Epi::setSetting('exceptions', true);
Epi::setPath('base', 'src');
Epi::setPath('view', './views');
Epi::init('route','template','session');

getRoute()->get('/', array('HomeController', 'display'));
getRoute()->run();
?>
